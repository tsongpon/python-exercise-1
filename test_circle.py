import unittest

from circle import Circle


class TestCircle(unittest.TestCase):
    def test_get_area(self):
        circle = Circle(10)
        self.assertEqual(314.1592653589793, circle.get_area(),
                         "area of circle with redis 10 should be 314.1592653589793")

    def test_get_perimeter(self):
        circle = Circle(10)
        self.assertEqual(62.83185307179586, circle.get_perimeter(),
                         "perimeter of circle with redis 10 should be 62.83185307179586")


if __name__ == '__main__':
    unittest.main()
